package mil.niwc.sigman.util.json_files;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Converts an object to a Json string and saves it to a file.
 *
 * @param <T>
 */
public class JsonFileWriter<T> extends JsonFile<T> {
  private static final Logger logger = LogManager.getLogger();

  private final ObjectMapper objectMapper = new ObjectMapper();

  private BufferedWriter writer;

  public JsonFileWriter(final Class<T> classType, final String fileName) {
    super(classType, fileName);
  }

  public void overwrite(T model) {
    createNewJsonFile();
    openFileForOverwrite();
    writeObjectToJsonFile(model);
    closeJsonFile();
  }

  public void append(T model) {
    openFileForAppend();
    appendObjectToJsonFile(model);
    closeJsonFile();
  }

  private void openFileForOverwrite() {
    try {
      writer = new BufferedWriter(new FileWriter(FILE_PATH + fileName));
    } catch (IOException e) {
      logger.error(String.format("\n\nJsonFileWriter.openFileForWriting() failed to open %s for overwrite", FILE_PATH + fileName), e);
    }
  }

  private void openFileForAppend() {
    try {
      writer = new BufferedWriter(new FileWriter(FILE_PATH + fileName, true));
    } catch (IOException e) {
      logger.error(String.format("\n\nJsonFileWriter.openFileForAppend() failed to open %s for append", FILE_PATH + fileName), e);
    }
  }

  private void writeObjectToJsonFile(T model) {
    try {
      writer.write(convertObjectToJson(model));
    } catch (IOException e) {
      logger.error(String.format("\n\nJsonFileWriter.writeObjectToJsonFile() Failed to write json to %s.",  FILE_PATH + fileName), e);
    }
  }

  private void appendObjectToJsonFile(T model) {
    try {
      writer.append(convertObjectToJson(model));
    } catch (IOException e) {
      logger.error(String.format("\n\nJsonFileWriter.appendObjectToJsonFile() Failed to append json to %s.",  FILE_PATH + fileName), e);
    }
  }

  private String convertObjectToJson(T model) {
    String json = "";
    try {
      json = objectMapper.writeValueAsString(model);
    } catch (JsonProcessingException e) {
      logger.error(String.format("\n\nJsonFileWriter.convertObjectToJson() failed to create json string from object. %s",  model), e);
    }
    return json;
  }

  private void closeJsonFile(){
    try {
      writer.close();
    } catch (IOException e) {
      logger.error(String.format("\n\nJsonFileWriter.closeJsonFile() Failed to close %s.",  FILE_PATH + fileName), e);
    }
  }
}
