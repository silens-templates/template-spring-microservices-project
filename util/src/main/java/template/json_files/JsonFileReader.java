package mil.niwc.sigman.util.json_files;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Reads a Json string from a file and converts it to an object.
 *
 * @param <T>
 */
public class JsonFileReader<T> extends JsonFile<T> {
  private static final Logger logger = LogManager.getLogger();

  private final ObjectMapper objectMapper = new ObjectMapper();

  private BufferedReader reader;

  private String jsonString;

  private T model;

  public JsonFileReader(final Class<T> classType, final String fileName) {
    super(classType, fileName);
  }

  public T load() {
    openFileForReading();
    readJsonFromFile();
    closeFile();

    return convertJsonToModel();
  }

  private void openFileForReading() {
    try {
      reader = new BufferedReader(new FileReader(FILE_PATH + fileName));
    } catch (IOException e) {
      logger.error(String.format("\n\nJsonFileReader.openFileForReading() failed to open %s for reading", FILE_PATH + fileName), e);
    }
  }

  private void readJsonFromFile() {
    jsonString = "";
    String jsonSingleLine = "";
    try {
      while ((jsonSingleLine = reader.readLine()) != null) {
        jsonString = jsonString + jsonSingleLine;
      }
    } catch (IOException e) {
      logger.error(String.format("\n\nJsonFileReader.readJsonFromFile() failed to read line from %s", FILE_PATH + fileName), e);
    }
  }

  private void closeFile() {
    try {
      reader.close();
    } catch (IOException e) {
      logger.error(String.format("\n\nJsonFileReader.closeFile() failed to close %s", FILE_PATH + fileName), e);
    }
  }

  private T convertJsonToModel() {
    if (jsonString != null) {
      try {
        model = objectMapper.readValue(jsonString, classType);
      } catch (JsonParseException e) {
        logger.error(String.format("\n\nJsonFileReader.convertJsonToModel() json does not conform to JSON syntax"), e);
      } catch (JsonMappingException e) {
        logger.error(String.format("\n\nJsonFileReader.convertJsonToModel() failed to map json to object"), e);
      } catch (JsonProcessingException e) {
        logger.error(String.format("\n\nJsonFileReader.convertJsonToModel() problems encountered when processing (parsing, generating) JSON "), e);
      }
    }
    return model;
  }
}
