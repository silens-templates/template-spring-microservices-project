package mil.niwc.sigman.util.json_files.exceptions;

public class JsonFileFailedToMakeFileWritableException extends RuntimeException {
  public JsonFileFailedToMakeFileWritableException() {
  }

  public JsonFileFailedToMakeFileWritableException(String message) {
    super(message);
  }

  public JsonFileFailedToMakeFileWritableException(String message, Throwable cause) {
    super(message, cause);
  }

  public JsonFileFailedToMakeFileWritableException(Throwable cause) {
    super(cause);
  }
}