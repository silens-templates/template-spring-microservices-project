package mil.niwc.sigman.util.json_files.exceptions;

public class JsonFileFailedToCreateDirectoryException extends RuntimeException {
  public JsonFileFailedToCreateDirectoryException() {
  }

  public JsonFileFailedToCreateDirectoryException(String message) {
    super(message);
  }

  public JsonFileFailedToCreateDirectoryException(String message, Throwable cause) {
    super(message, cause);
  }

  public JsonFileFailedToCreateDirectoryException(Throwable cause) {
    super(cause);
  }
}