package mil.niwc.sigman.util.json_files.exceptions;

public class JsonFileFailedToDeleteFileException extends RuntimeException {
  public JsonFileFailedToDeleteFileException() {
  }

  public JsonFileFailedToDeleteFileException(String message) {
    super(message);
  }

  public JsonFileFailedToDeleteFileException(String message, Throwable cause) {
    super(message, cause);
  }

  public JsonFileFailedToDeleteFileException(Throwable cause) {
    super(cause);
  }
}