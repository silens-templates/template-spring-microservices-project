package mil.niwc.sigman.util.json_files;

import mil.niwc.sigman.util.json_files.exceptions.JsonFileFailedToCreateDirectoryException;
import mil.niwc.sigman.util.json_files.exceptions.JsonFileFailedToDeleteFileException;
import mil.niwc.sigman.util.json_files.exceptions.JsonFileFailedToMakeFileWritableException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;

public abstract class JsonFile<T> {
  private static final Logger logger = LogManager.getLogger();

  protected final String FILE_PATH = System.getProperty("user.home") + "\\AppData\\Local\\Fantom\\Sensor\\";

  protected final String fileName;

  protected final Class<T> classType;

  protected final File file;

  public JsonFile(final Class<T> classType, final String fileName) {
    this.fileName = fileName;
    this.classType = classType;
    this.file = new File(FILE_PATH + fileName);

    createNewFileIfItDoesNotExist();
  }

  private void createNewFileIfItDoesNotExist() {
    if (file.exists()) {
      //Do nothing
    } else {
      createDirectory();
      createFile();
      makeFileWritable();
    }
  }

  private void createDirectory() {
    try {
      tryToCreateDirectory();
    } catch (JsonFileFailedToCreateDirectoryException e) {
      logger.error(String.format("\n\nJsonFile.createDirectory() failed to create %s", file.getAbsolutePath()), e);
    }
  }

  private void tryToCreateDirectory() throws JsonFileFailedToCreateDirectoryException {
    boolean successfullyCreatedDirectory = file.getParentFile().mkdirs();

    if (successfullyCreatedDirectory) {
      //Do nothing
    } else {
      throw new JsonFileFailedToCreateDirectoryException("JsonFile.createDirectory() failed to create the file directory.");
    }
  }

  private void createFile() {
    try {
      file.createNewFile();
    } catch (IOException e) {
      logger.error(String.format("\n\nJsonFile.createFile() failed to create %s", file.getAbsoluteFile()), e);
    }
  }

  private void makeFileWritable() {
    try {
      tryToMakeFileWritable();
    } catch (JsonFileFailedToMakeFileWritableException e) {
      logger.error(String.format("\n\nJsonFile.createFile() User does not have permission to write to %s", file.getAbsoluteFile()), e);
    }
  }

  private void tryToMakeFileWritable() throws JsonFileFailedToMakeFileWritableException {
    boolean successfullyMadeFileWritable = file.setWritable(true);

    if (successfullyMadeFileWritable) {
      //Do nothing
    } else {
      throw new JsonFileFailedToMakeFileWritableException("JsonFile.makeFileWritable() failed to make file writable, because the user does not have permission to change the access permissions of this abstract pathname.");
    }
  }

  public void createNewJsonFile() {
    deleteJsonFile();
    createNewFileIfItDoesNotExist();
  }

  private void deleteJsonFile() {
    try {
      tryToDeleteFile();
    } catch (JsonFileFailedToDeleteFileException e) {
      logger.error(String.format("\n\nJsonFile.deleteJsonFile() failed to delete %s", file.getAbsoluteFile()), e);
    }
  }

  private void tryToDeleteFile() throws JsonFileFailedToDeleteFileException {
    if (file.exists()) {
      boolean successfullyDeletedFile = file.delete();

      if (successfullyDeletedFile) {
        //Do nothing
      } else {
        throw new JsonFileFailedToDeleteFileException("JsonFile.tryToDeleteFile() failed to delete.");
      }
    }
  }
}
