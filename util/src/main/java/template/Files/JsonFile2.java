package template.Files;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class JsonFile2<T> {
  private static final Logger logger = LogManager.getLogger();

  private final ObjectMapper objectMapper = new ObjectMapper();

  public final String FILE_PATH = System.getProperty("user.home") + "\\AppData\\Local\\Fantom\\Sensor\\";

  public final String fileName;

  public final Class<T> classType;

  public final File file;

  public JsonFile2(final Class<T> classType, final String fileName) {
    this.fileName = fileName;
    this.classType = classType;

    file = new File(FILE_PATH + fileName);
    if (file.exists()) {

    } else {
      try {
        file.getParentFile().mkdirs();
        file.createNewFile();
      } catch (IOException e) {
        e.printStackTrace();
      }
      file.setWritable(true);
    }
  }

  public boolean save(T model) {
    boolean saveSuccess = true;

    BufferedWriter writer = null;
    try {
      writer = new BufferedWriter(new FileWriter(FILE_PATH + fileName));
    } catch (IOException e) {
      logger.error(String.format("Failed to open %s for write", FILE_PATH + fileName));
      saveSuccess = false;
      e.printStackTrace();
    }

    try {
      writer.write(objectMapper.writeValueAsString(model));
    } catch (IOException e) {
      logger.error(String.format("Failed to convert model to json. %s", model.toString()));
      saveSuccess = false;
      e.printStackTrace();
    }

    try {
      writer.close();
    } catch (IOException e) {
      logger.error(String.format("Failed to close %s", FILE_PATH + fileName));
      saveSuccess = false;
      e.printStackTrace();
    }

    return saveSuccess;
  }

  public boolean append(T model) {
    boolean saveSuccess = true;

    BufferedWriter writer = null;
    try {
      writer = new BufferedWriter(new FileWriter(FILE_PATH + fileName, true));
    } catch (IOException e) {
      logger.error(String.format("Failed to open %s for write", FILE_PATH + fileName));
      saveSuccess = false;
      e.printStackTrace();
    }

    try {
      writer.append(objectMapper.writeValueAsString(model) + "\n");
    } catch (IOException e) {
      logger.error(String.format("Failed to convert model to json. %s", model.toString()));
      saveSuccess = false;
      e.printStackTrace();
    }

    try {
      writer.close();
    } catch (IOException e) {
      logger.error(String.format("Failed to close %s", FILE_PATH + fileName));
      saveSuccess = false;
      e.printStackTrace();
    }

    return saveSuccess;
  }

  public T load() {

    BufferedReader reader = null;
    try {
      reader = new BufferedReader(new FileReader(FILE_PATH + fileName));
    } catch (FileNotFoundException e) {
      logger.error(String.format("Failed to open %s for read", FILE_PATH + fileName));
      e.printStackTrace();
    }

    String jsonString = "";
    String jsonSingleLine = "";
    try {
      while ((jsonSingleLine = reader.readLine()) != null) {
        jsonString = jsonString + jsonSingleLine;
      }
    } catch (IOException e) {
      e.printStackTrace();
    }

    T model = null;

    if (jsonString != null) {
      try {
        model = objectMapper.readValue(jsonString, classType);
      } catch (IOException e) {
        logger.error(String.format("Failed to convert file json to model."));
        e.printStackTrace();
      }
    }

    try {
      reader.close();
    } catch (IOException e) {
      logger.error(String.format("Failed to close %s", FILE_PATH + fileName));
      e.printStackTrace();
    }

    return model;
  }

  public void clear() {
    file.delete();

    try {
      file.getParentFile().mkdirs();
      file.createNewFile();
    } catch (IOException e) {
      e.printStackTrace();
    }
    file.setWritable(true);
  }
}
