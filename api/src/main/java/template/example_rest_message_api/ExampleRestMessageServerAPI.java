package template.example_rest_message_api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Api(tags = "Rest Message")
public interface ExampleRestMessageServerAPI {
  @ApiOperation("Post rest message from gateway-service to process-service and wait for reply")
  @RequestMapping(path = "/post/rest_message", method = RequestMethod.POST)
  ResponseEntity<RestMessageDTO> postRestMessage(@RequestBody RestMessageDTO restMessageDTO);

  @ApiOperation("Get rest message from gateway-service")
  @RequestMapping(path = "/get/rest_message", method = RequestMethod.GET)
  ResponseEntity<RestMessageDTO> getRestMessage();
}
