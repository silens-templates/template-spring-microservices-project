package template.example_rest_message_api;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@ToString
@RequiredArgsConstructor
public class RestMessageDTO {

  @NonNull
  private final List<String> messages;

  public RestMessageDTO() {
    this(new ArrayList<>());
  }
}