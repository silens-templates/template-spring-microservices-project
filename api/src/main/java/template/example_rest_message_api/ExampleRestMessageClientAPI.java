package template.example_rest_message_api;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

public interface ExampleRestMessageClientAPI {
  @HystrixCommand(fallbackMethod = "postRestMessageFallback",
      // Circuit Breaker
      commandProperties = {
          @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "600000"),
          @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "5"),
          @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "50"),
          @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
      },
      // Bulkhead Pattern
      threadPoolKey = "text-pool", // create new tread pool bulkhead
      threadPoolProperties = {
          @HystrixProperty(name = "coreSize", value = "20"), // number of concurrent threads
          @HystrixProperty(name = "maxQueueSize", value = "10"), // how many request to queue without consuming thread resources or triggering error
      })
  public RestMessageDTO postRestMessage(RestMessageDTO restMessageDTO);
}