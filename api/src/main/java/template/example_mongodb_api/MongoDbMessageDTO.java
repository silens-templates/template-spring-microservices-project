package template.example_mongodb_api;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor
public class MongoDbMessageDTO {
  private final int field1;
  private final long field2;
  private final float field3;
  private final double field4;

  @NonNull
  private final String field5;

  @NonNull
  private final List<String> messages;

  public MongoDbMessageDTO() {
    this(
        0,
        0l,
        0.0f,
        0.0,
        "",
        new ArrayList<>()
    );
  }
}