package template.example_mongodb_api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import template.example_rabbitmq_message_api.RabbitmqMessageDTO;

@Api(tags = "Mongo Database Message")
public interface ExampleMongoDbMessageAPI {
  @ApiOperation("POST MongoDB Message to database")
  @RequestMapping(path = "/post/mongodb_message", method = RequestMethod.POST)
  ResponseEntity<MongoDbMessageDTO> postMongoDbMessage(@RequestBody MongoDbMessageDTO mongoDbMessageDTO);

  @ApiOperation("POST Clear MongoDB Message database")
  @RequestMapping(path = "/post/clear_mongo_database", method = RequestMethod.POST)
  ResponseEntity<Boolean> postClearMongoDatabase();
}