package template.process_service_clients;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import template.example_rabbitmq_message_api.RabbitmqMessageDTO;

public interface SendExample1API {
  @HystrixCommand(fallbackMethod = "postSendExample1APIFallbackDTO",
      // Circuit Breaker
      commandProperties = {
          @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "600000"),
          @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "5"),
          @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "50"),
          @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
      },
      // Bulkhead Pattern
      threadPoolKey = "text-pool", // create new tread pool bulkhead
      threadPoolProperties = {
          @HystrixProperty(name = "coreSize", value = "20"), // number of concurrent threads
          @HystrixProperty(name = "maxQueueSize", value = "10"), // how many request to queue without consuming thread resources or triggering error
      })
  public Boolean sendExample1API(RabbitmqMessageDTO rabbitmqMessageDTO);
}