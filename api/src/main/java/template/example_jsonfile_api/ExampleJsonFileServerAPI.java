package template.example_jsonfile_api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Api(tags = "JsonFile Message")
public interface ExampleJsonFileServerAPI {
  @ApiOperation("Post json to save json to file then load json from file for return")
  @RequestMapping(path = "/post/json_file_message", method = RequestMethod.POST)
  ResponseEntity<JsonFileMessageDTO> postJsonFileMessage(@RequestBody JsonFileMessageDTO jsonFileMessageDTO);
}