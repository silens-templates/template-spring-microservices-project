package template.example_jsonfile_api;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@ToString
@RequiredArgsConstructor
public class JsonFileMessageDTO {

  @NonNull
  private final List<String> messages;

  public JsonFileMessageDTO() {
    this(new ArrayList<>());
  }
}