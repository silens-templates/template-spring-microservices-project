package template;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.concurrent.TimeUnit;

/**
 * Represents a GPS timestamp
 */
@Builder
@ToString
@RequiredArgsConstructor
public class TimestampUTC {
  /**
   * The number of nanoseconds since 1/1/1970 UTC. This can hold times until about July of 2554
   */
  @JsonProperty("time_in_nanoseconds")
  protected final long timeInNanoSeconds;

  public TimestampUTC() {
    this(0l);
  }

  public long getTimeInNanoSeconds() {
    return timeInNanoSeconds;
  }

  @JsonIgnore
  public long getTimeInMicroSeconds() {
    return TimeUnit.MICROSECONDS.convert(timeInNanoSeconds, TimeUnit.NANOSECONDS);
  }

  @JsonIgnore
  public long getTimeInMilliSeconds() {
    return TimeUnit.MILLISECONDS.convert(timeInNanoSeconds, TimeUnit.NANOSECONDS);
  }

  @JsonIgnore
  public long getTimeInSeconds() {
    return TimeUnit.SECONDS.convert(timeInNanoSeconds, TimeUnit.NANOSECONDS);
  }

  @JsonIgnore
  public long getTimeInMinutes() {
    return TimeUnit.MINUTES.convert(timeInNanoSeconds, TimeUnit.NANOSECONDS);
  }

  @JsonIgnore
  public long getTimeInHours() {
    return TimeUnit.HOURS.convert(timeInNanoSeconds, TimeUnit.NANOSECONDS);
  }

  @JsonIgnore
  public long getTimeInDays() {
    return TimeUnit.DAYS.convert(timeInNanoSeconds, TimeUnit.NANOSECONDS);
  }
}
