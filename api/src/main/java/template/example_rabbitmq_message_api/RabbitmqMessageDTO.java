package template.example_rabbitmq_message_api;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@ToString
@RequiredArgsConstructor
public class RabbitmqMessageDTO {

  @NonNull
  private final List<String> messages;

  public RabbitmqMessageDTO() {
    this(new ArrayList<>());
  }
}