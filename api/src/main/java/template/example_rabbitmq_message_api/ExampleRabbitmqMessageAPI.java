package template.example_rabbitmq_message_api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Api(tags = "RabbitMQ Message")
public interface ExampleRabbitmqMessageAPI {
  @ApiOperation("Send message from gateway-service to process-service via RabbitMQ")
  @RequestMapping(path = "/post/rabbitmq_message", method = RequestMethod.POST)
  ResponseEntity<Boolean> postRabbitMqMessage(@RequestBody RabbitmqMessageDTO rabbitmqMessageDTO);
}