package template.example_websocket_message_api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Api(tags = "Socket Message")
public interface ExampleSocketMessageAPI {
  @ApiOperation("Send a message from the gateway-service to the process-service via sockets")
  @RequestMapping(path = "/post/socket_message", method = RequestMethod.POST)
  ResponseEntity<Boolean> postSocketMessage(@RequestBody SocketMessageDTO socketMessageDTO);
}