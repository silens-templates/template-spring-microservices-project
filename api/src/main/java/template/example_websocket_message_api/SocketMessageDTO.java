package template.example_websocket_message_api;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@ToString
@RequiredArgsConstructor
public class SocketMessageDTO {

  @NonNull
  private final List<String> messages;

  public SocketMessageDTO() {
    this(new ArrayList<>());
  }
}
