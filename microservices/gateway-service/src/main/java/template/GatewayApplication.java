package template;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.context.ConfigurableApplicationContext;

import static template.configuration.Constants.SPRINT_VERSION;

/**
 * java -Dserver.port=8201 -jar language-id-0.0.1.jar
 * java -jar speaker-id-0.0.1.jar
 */

@SpringBootApplication
@EnableEurekaClient
@EnableCircuitBreaker
@EnableHystrixDashboard
public class GatewayApplication {
  private static final Logger logger = LogManager.getLogger();

  public static void main(String[] args) {
    ConfigurableApplicationContext context = SpringApplication.run(GatewayApplication.class, args);

    logger.info(String.format("\n\n"));
    logger.info(String.format("--------------------------------------------------------------"));
    logger.info(String.format("VERSION = %s", SPRINT_VERSION));
  }
}