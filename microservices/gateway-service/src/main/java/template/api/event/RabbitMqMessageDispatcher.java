package template.api.event;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import template.example_rabbitmq_message_api.RabbitmqMessageDTO;

/**
 * Handles the communication with the Event Bus.
 */
@Component
public class RabbitMqMessageDispatcher {
  private static final Logger logger = LogManager.getLogger();

  private RabbitTemplate rabbitTemplate;

  // The exchange to use to send anything related to Template-Services
  private String fantomExchange;

  // The routing key to use to send this particular event
  private String connectionStatusKey;

  @Autowired
  RabbitMqMessageDispatcher(final RabbitTemplate rabbitTemplate,
                            @Value("${template.exchange}") final String fantomExchange,
                            @Value("${example.rabbitmq.message.key}") final String connectionStatusKey) {
    this.rabbitTemplate = rabbitTemplate;
    this.fantomExchange = fantomExchange;
    this.connectionStatusKey = connectionStatusKey;
  }

  public void send(final RabbitmqMessageDTO rabbitmqMessageDTO) {

    rabbitmqMessageDTO.getMessages().add(String.format("gateway.RabbitMqMessageDispatcher.send()\n"));
    logger.info(rabbitmqMessageDTO);

    rabbitTemplate.convertAndSend(
        fantomExchange,
        connectionStatusKey,
        rabbitmqMessageDTO);
  }
}