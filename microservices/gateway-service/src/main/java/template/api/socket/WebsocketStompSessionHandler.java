package template.api.socket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.messaging.simp.stomp.*;
import org.springframework.stereotype.Component;
import template.example_websocket_message_api.SocketMessageDTO;

import java.lang.reflect.Type;

@Component
public class WebsocketStompSessionHandler extends StompSessionHandlerAdapter  implements StompSessionHandler {
  private Logger logger = LogManager.getLogger(WebsocketStompSessionHandler.class);

  @Override
  public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
    logger.info("New session established : " + session.getSessionId());

    session.subscribe("/topic/messages", this);
    logger.info("Subscribed to /topic/messages");
  }

  @Override
  public void handleException(StompSession session, StompCommand command, StompHeaders headers, byte[] payload, Throwable exception) {
    logger.error("Got an exception", exception);
  }

  @Override
  public Type getPayloadType(StompHeaders headers) {
    return SocketMessageDTO.class;
  }

  @Override
  public void handleFrame(StompHeaders headers, Object payload) {
    SocketMessageDTO socketMessageDTO = (SocketMessageDTO) payload;

    socketMessageDTO.getMessages().add(String.format("gateway.WebsocketStompSessionHandler.handleFrame()\n"));
    logger.info(socketMessageDTO);
  }
}