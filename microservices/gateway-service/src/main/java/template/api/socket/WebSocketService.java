package template.api.socket;

import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.stereotype.Service;
import template.configuration.WebSocketConfig;
import template.example_websocket_message_api.SocketMessageDTO;

@Getter
@Service
public class WebSocketService {
  private static final Logger logger = LogManager.getLogger();

  private StompSession stompSession;
  private WebSocketConfig webSocketConfig;

  public WebSocketService(WebSocketConfig webSocketConfig) {
    this.webSocketConfig = webSocketConfig;
    this.initSession();
  }

  public void initSession() {
    logger.info(String.format("WebSocketService.initSession()"));

    try {
      this.stompSession = this.webSocketConfig.initSession();
    } catch (Exception e) {
      logger.info("Create stomp session failed ", e);
      this.stompSession = null;
    }
  }

  public void send(SocketMessageDTO socketMessageDTO){

    socketMessageDTO.getMessages().add(String.format("gateway.WebSocketService.send()\n"));

    stompSession.send("/app/socket/socket_message", socketMessageDTO);
  }

}