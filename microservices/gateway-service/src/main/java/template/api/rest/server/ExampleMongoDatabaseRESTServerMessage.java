package template.api.rest.server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import template.api.rest.client.ProcessServiceRESTClient;
import template.example_mongodb_api.ExampleMongoDbMessageAPI;
import template.example_mongodb_api.MongoDbMessageDTO;
import template.services.GatewayService;

@Service
@RestController
public class ExampleMongoDatabaseRESTServerMessage implements ExampleMongoDbMessageAPI {
  private static final Logger logger = LogManager.getLogger();

  @Autowired
  private GatewayService gatewayService;

  @Override
  public ResponseEntity<MongoDbMessageDTO> postMongoDbMessage(@RequestBody MongoDbMessageDTO mongoDbMessageDTO) {
    return loadMongoDbMessageFromDatabase(saveMongoDbMessageToDatabase(mongoDbMessageDTO));
  }

  private MongoDbMessageDTO saveMongoDbMessageToDatabase(MongoDbMessageDTO mongoDbMessageDTO) {
    return gatewayService.saveMongoDbMessage(mongoDbMessageDTO);
  }

  private ResponseEntity<MongoDbMessageDTO> loadMongoDbMessageFromDatabase(MongoDbMessageDTO mongoDbMessageDTO) {
    return new ResponseEntity<MongoDbMessageDTO>(mongoDbMessageDTO, null, HttpStatus.OK);
  }

  @Override
  public ResponseEntity<Boolean> postClearMongoDatabase() {
    return acknowledgeSuccessClearMongoDatabasePost(handleClearMongoDatabasePost());
  }

  private Boolean handleClearMongoDatabasePost() {
    return gatewayService.clearDatabase();
  }

  private ResponseEntity<Boolean> acknowledgeSuccessClearMongoDatabasePost(Boolean acknowledgeSuccess) {
    return new ResponseEntity<Boolean>(acknowledgeSuccess, null, HttpStatus.OK);
  }
}