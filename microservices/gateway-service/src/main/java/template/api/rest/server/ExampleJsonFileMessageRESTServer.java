package template.api.rest.server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import template.example_jsonfile_api.ExampleJsonFileServerAPI;
import template.example_jsonfile_api.JsonFileMessageDTO;
import template.services.GatewayService;

@Service
@RestController
public class ExampleJsonFileMessageRESTServer implements ExampleJsonFileServerAPI {
  private static final Logger logger = LogManager.getLogger();

  @Autowired
  private GatewayService gatewayService;

  @Override
  public ResponseEntity<JsonFileMessageDTO> postJsonFileMessage(@RequestBody JsonFileMessageDTO jsonFileMessageDTO) {

    jsonFileMessageDTO.getMessages().add(String.format("gateway.ExampleJsonFileMessageRESTServer.postJsonFileMessage()\n"));

    return respondToJsonFileMessagePost(handleJsonFileMessagePost(jsonFileMessageDTO));
  }

  private JsonFileMessageDTO handleJsonFileMessagePost(JsonFileMessageDTO jsonFileMessageDTO) {
    return gatewayService.updateJsonFileMessage(jsonFileMessageDTO);
  }

  private ResponseEntity<JsonFileMessageDTO> respondToJsonFileMessagePost(JsonFileMessageDTO jsonFileMessageDTO) {
    return new ResponseEntity<JsonFileMessageDTO>(jsonFileMessageDTO, null, HttpStatus.OK);
  }
}