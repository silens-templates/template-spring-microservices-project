package template.api.rest.client;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import template.example_rabbitmq_message_api.RabbitmqMessageDTO;
import template.example_rest_message_api.ExampleRestMessageClientAPI;
import template.example_rest_message_api.RestMessageDTO;
import template.process_service_clients.ClearDatabaseAPI;
import template.process_service_clients.SendExample1API;

@Service
public class ProcessServiceRESTClient implements ClearDatabaseAPI, SendExample1API, ExampleRestMessageClientAPI {
  private static final Logger logger = LogManager.getLogger();

  @Autowired
  private RestTemplate restTemplate;

  @Override
  public boolean clearDatabase() {
    return sendClearDatabaseToProcessService();
  }

  private boolean sendClearDatabaseToProcessService() {
    ResponseEntity<Boolean> response = restTemplate.postForEntity("http://process-service/post/clear_database", null, Boolean.class);

    return response.getBody();
  }

  private boolean postClearDatabaseFallbackDTO() {
    return false;
  }

  @Override
  public Boolean sendExample1API(RabbitmqMessageDTO rabbitmqMessageDTO) {
    logger.info("Send example 1 to process-service" + rabbitmqMessageDTO.toString());
    return sendExample1ToProcessService(rabbitmqMessageDTO);
  }

  private Boolean sendExample1ToProcessService(RabbitmqMessageDTO rabbitmqMessageDTO) {
    ResponseEntity<Boolean> response = restTemplate.postForEntity("http://process-service/post/example_one", rabbitmqMessageDTO, Boolean.class);

    return response.getBody();
  }

  private RabbitmqMessageDTO postSendExample1APIFallbackDTO(RabbitmqMessageDTO rabbitmqMessageDTO) {
    return new RabbitmqMessageDTO();
  }


  @Override
  public RestMessageDTO postRestMessage(RestMessageDTO restMessageDTO) {
    restMessageDTO.getMessages().add(String.format("gateway.ProcessServiceRESTClient.postRestMessage()\n"));

    RestMessageDTO responseRestMessage = sendRestMessageToProcessService(restMessageDTO);

    responseRestMessage.getMessages().add(String.format("gateway.ProcessServiceRESTClient.postRestMessage()\n"));
    logger.info(responseRestMessage);

    return responseRestMessage;
  }

  private RestMessageDTO sendRestMessageToProcessService(RestMessageDTO restMessageDTO) {
    ResponseEntity<RestMessageDTO> response = restTemplate.postForEntity("http://process-service/post/rest_message", restMessageDTO, RestMessageDTO.class);

    return response.getBody();
  }

  private RestMessageDTO postRestMessageFallback(RestMessageDTO restMessageDTO) {
    return new RestMessageDTO();
  }
}