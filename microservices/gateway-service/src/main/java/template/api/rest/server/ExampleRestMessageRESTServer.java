package template.api.rest.server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import template.example_rest_message_api.ExampleRestMessageServerAPI;
import template.example_rest_message_api.RestMessageDTO;
import template.services.GatewayService;

@Service
@RestController
public class ExampleRestMessageRESTServer implements ExampleRestMessageServerAPI {
  private static final Logger logger = LogManager.getLogger();

  @Autowired
  private GatewayService gatewayService;

  @Override
  public ResponseEntity<RestMessageDTO> postRestMessage(@RequestBody RestMessageDTO restMessageDTO) {

    restMessageDTO.getMessages().add(String.format("gateway.ExampleRestMessageRESTServer.postRestMessage()\n"));

    return acknowledgeReceivedRestMessagePost(handleRestMessagePost(restMessageDTO));
  }

  private RestMessageDTO handleRestMessagePost(RestMessageDTO restMessageDTO) {
    return gatewayService.updateRestMessage(restMessageDTO);
  }

  private ResponseEntity<RestMessageDTO> acknowledgeReceivedRestMessagePost(RestMessageDTO restMessageDTO) {
    return new ResponseEntity<RestMessageDTO>(restMessageDTO, null, HttpStatus.OK);
  }

  @Override
  public ResponseEntity<RestMessageDTO> getRestMessage() {
    RestMessageDTO restMessageDTO = new RestMessageDTO();
    restMessageDTO.getMessages().add(String.format("gateway.ExampleRestMessageRESTServer.getRestMessage()\n"));
    logger.info(restMessageDTO);

    return returnRestMessage(restMessageDTO);
  }

  private ResponseEntity<RestMessageDTO> returnRestMessage(RestMessageDTO restMessageDTO) {
    return new ResponseEntity<RestMessageDTO>(restMessageDTO, null, HttpStatus.OK);
  }
}