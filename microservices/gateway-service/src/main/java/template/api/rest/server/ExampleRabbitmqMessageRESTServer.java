package template.api.rest.server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import template.example_rabbitmq_message_api.ExampleRabbitmqMessageAPI;
import template.example_rabbitmq_message_api.RabbitmqMessageDTO;
import template.services.GatewayService;

@Service
@RestController
public class ExampleRabbitmqMessageRESTServer implements ExampleRabbitmqMessageAPI {
  private static final Logger logger = LogManager.getLogger();

  @Autowired
  private GatewayService gatewayService;

  @Override
  public ResponseEntity<Boolean> postRabbitMqMessage(@RequestBody RabbitmqMessageDTO rabbitmqMessageDTO){

    rabbitmqMessageDTO.getMessages().add(String.format("gateway.ExampleRabbitmqMessageRESTServer.postRabbitMqMessage()\n"));

    return acknowledgeReceivedRabbitMqMessagePost(handleRabbitmqMessagePost(rabbitmqMessageDTO));
  }

  private Boolean handleRabbitmqMessagePost(RabbitmqMessageDTO rabbitmqMessageDTO) {
    return gatewayService.updateRabbitMqMessage(rabbitmqMessageDTO);
  }

  private ResponseEntity<Boolean> acknowledgeReceivedRabbitMqMessagePost(Boolean acknowledgeSuccess) {
    return new ResponseEntity<Boolean>(acknowledgeSuccess, null, HttpStatus.OK);
  }
}