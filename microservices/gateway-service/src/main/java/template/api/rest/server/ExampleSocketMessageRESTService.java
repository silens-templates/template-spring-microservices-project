package template.api.rest.server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import template.example_websocket_message_api.ExampleSocketMessageAPI;
import template.example_websocket_message_api.SocketMessageDTO;
import template.services.GatewayService;

@Service
@RestController
public class ExampleSocketMessageRESTService implements ExampleSocketMessageAPI {
  private static final Logger logger = LogManager.getLogger();

  @Autowired
  private GatewayService gatewayService;

  @Override
  public ResponseEntity<Boolean> postSocketMessage(@RequestBody SocketMessageDTO socketMessageDTO) {

    socketMessageDTO.getMessages().add(String.format("gateway.ExampleSocketMessageRESTService.postSocketMessage()\n"));

    return acknowledgeReceivedSocketMessagePost(handleSocketMessage(socketMessageDTO));
  }

  private Boolean handleSocketMessage(SocketMessageDTO socketMessageDTO) {
    return gatewayService.updateSocketMessage(socketMessageDTO);
  }

  private ResponseEntity<Boolean> acknowledgeReceivedSocketMessagePost(Boolean acknowledgeSuccess) {
    return new ResponseEntity<Boolean>(acknowledgeSuccess, null, HttpStatus.OK);
  }
}