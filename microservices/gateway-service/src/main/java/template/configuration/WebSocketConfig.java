package template.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import template.api.socket.WebsocketStompSessionHandler;

@Configuration
public class WebSocketConfig {
  private static final Logger logger = LogManager.getLogger();

  @Value("${ws.server.host}")
  private String serverURL;

  private final StompSessionHandler sessionHandler = new WebsocketStompSessionHandler();

  @Retryable(value = {Exception.class},
      maxAttempts = 100,
      backoff = @Backoff(delay = 1000, multiplier = 2, random = true))
  public StompSession initSession() throws Exception {
    logger.info(String.format("WebSocketConfig.initSession()"));

    return stompClient().connect(serverURL, sessionHandler).get();
  }

  @Bean
  public WebSocketStompClient stompClient() {
    logger.info(String.format("WebSocketConfig.stompClient()"));

    WebSocketClient simpleWebSocketClient = new StandardWebSocketClient();

    WebSocketStompClient stompClient = new WebSocketStompClient(simpleWebSocketClient);

    stompClient.setMessageConverter(new MappingJackson2MessageConverter());

    ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();

    scheduler.initialize();

    stompClient.setTaskScheduler(scheduler);

    return stompClient;
  }
}