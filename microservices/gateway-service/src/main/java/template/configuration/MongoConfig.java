package template.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.util.Collection;
import java.util.Collections;

@Configuration
@EnableMongoRepositories(basePackages = "template")
public class MongoConfig extends AbstractMongoClientConfiguration {

  @Autowired
  private MappingMongoConverter mongoConverter;

  @Override
  protected String getDatabaseName() {
    return "gateway_db";
  }

  @Override
  public Collection<String> getMappingBasePackages() {
    return Collections.singleton("template");
  }

  @Bean
  public GridFsTemplate gridFsTemplate() throws Exception {
    return new GridFsTemplate(mongoDbFactory(), mongoConverter);
  }
}