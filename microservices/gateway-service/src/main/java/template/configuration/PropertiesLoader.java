package template.configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesLoader {
  public static Properties gateway()  {
    Properties properties = new Properties();
    InputStream inputStream = PropertiesLoader.class
        .getClassLoader()
        .getResourceAsStream("gateway.properties");

    try {
      properties.load(inputStream);
    } catch (IOException e) {
      e.printStackTrace();
    }

    try {
      inputStream.close();
    } catch (IOException e) {
      e.printStackTrace();
    }

    return properties;
  }
}
