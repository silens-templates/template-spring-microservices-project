package template.configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Public final static fields only
 * import static
 */
public final class Constants {
  private Constants() {
  }

  public final static String SPRINT_VERSION = PropertiesLoader.gateway().getProperty("version");

  public final static Integer EXAMPLE_INT_PROPERTY = Integer.parseInt(PropertiesLoader.gateway().getProperty("example_int_property"));
  public final static Long EXAMPLE_LONG_PROPERTY = Long.parseLong(PropertiesLoader.gateway().getProperty("example_long_property"));
  public final static Float EXAMPLE_FLOAT_PROPERTY = Float.parseFloat(PropertiesLoader.gateway().getProperty("example_float_property"));
  public final static Double EXAMPLE_DOUBLE_PROPERTY = Double.parseDouble(PropertiesLoader.gateway().getProperty("example_double_property"));

  public final static Boolean EXAMPLE_BOOLEAN_PROPERTY = Boolean.parseBoolean(PropertiesLoader.gateway().getProperty("example_boolean_property"));

  public final static List<String> EXAMPLE_STRING_LIST_PROPERTY = new ArrayList<String>(Arrays.asList(PropertiesLoader.gateway().getProperty("example_string_list_property").split(",")));
}