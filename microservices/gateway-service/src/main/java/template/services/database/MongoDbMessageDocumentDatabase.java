package template.services.database;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import template.models.MongoDbDocumentModel;

import java.util.List;

@Service
public class MongoDbMessageDocumentDatabase {
  private static final Logger logger = LogManager.getLogger();

  private final MongoDbMessageDocumentRepository mongoDbMessageDocumentRepository;

  @Autowired
  public MongoDbMessageDocumentDatabase(MongoDbMessageDocumentRepository mongoDbMessageDocumentRepository) {
    this.mongoDbMessageDocumentRepository = mongoDbMessageDocumentRepository;
  }

  public void save(MongoDbDocumentModel mongoDbDocumentModel) {
    if (mongoDbDocumentModel != null) {
      mongoDbMessageDocumentRepository.save(mongoDbDocumentModel);
    }
  }

  public void clear() {
    mongoDbMessageDocumentRepository.deleteAll();
  }

  public List<MongoDbDocumentModel> getAll() {
    return mongoDbMessageDocumentRepository.findAll();
  }

  public long size() {
    return mongoDbMessageDocumentRepository.count();
  }

  public boolean contains(MongoDbDocumentModel mongoDbDocumentModel) {
    return mongoDbMessageDocumentRepository.findByDoc1Id(mongoDbDocumentModel.getId()) != null;
  }

  public boolean hasChanged(final MongoDbDocumentModel mongoDbDocumentModel){
    if (mongoDbMessageDocumentRepository.existsById(mongoDbDocumentModel.getId())) {
      MongoDbDocumentModel currentEntry = mongoDbMessageDocumentRepository.findByDoc1Id(mongoDbDocumentModel.getId());

      return !currentEntry.equals(mongoDbDocumentModel);
    }else{
      return false;
    }
  }

  public MongoDbDocumentModel getEntry(ObjectId id) {
    return mongoDbMessageDocumentRepository.findByDoc1Id(id);
  }
}
