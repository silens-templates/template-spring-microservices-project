package template.services.database;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import template.models.MongoDbDocumentModel;

public interface MongoDbMessageDocumentRepository extends MongoRepository<MongoDbDocumentModel, ObjectId> {
  @Query(value = "{ 'id' : ?0 }")
  MongoDbDocumentModel findByDoc1Id(ObjectId id);
}