package template.services;

import com.google.gson.Gson;
import lombok.Synchronized;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import template.Files.JsonFile;
import template.api.event.RabbitMqMessageDispatcher;
import template.api.rest.client.ProcessServiceRESTClient;
import template.api.socket.WebSocketService;
import template.example_jsonfile_api.JsonFileMessageDTO;
import template.example_mongodb_api.MongoDbMessageDTO;
import template.example_rabbitmq_message_api.RabbitmqMessageDTO;
import template.example_rest_message_api.RestMessageDTO;
import template.example_websocket_message_api.SocketMessageDTO;
import template.models.MongoDbDocumentModel;
import template.services.database.MongoDbMessageDocumentDatabase;

@Service
public class GatewayService {
  private static final Logger logger = LogManager.getLogger();

  private Gson gson = new Gson();

  private final MongoDbMessageDocumentDatabase mongoDbMessageDocumentDatabase;

  private final RabbitMqMessageDispatcher rabbitMqMessageDispatcher;

  private final WebSocketService webSocketService;

  private final ProcessServiceRESTClient processServiceRESTClient;

  @Autowired
  public GatewayService(
      MongoDbMessageDocumentDatabase mongoDbMessageDocumentDatabase,
      RabbitMqMessageDispatcher rabbitMqMessageDispatcher,
      ProcessServiceRESTClient processServiceRESTClient,
      WebSocketService webSocketService
  ) {
    this.mongoDbMessageDocumentDatabase = mongoDbMessageDocumentDatabase;
    this.rabbitMqMessageDispatcher = rabbitMqMessageDispatcher;
    this.processServiceRESTClient = processServiceRESTClient;
    this.webSocketService = webSocketService;
  }

  @Synchronized
  public JsonFileMessageDTO updateJsonFileMessage(JsonFileMessageDTO jsonFileMessageDTO){

    jsonFileMessageDTO.getMessages().add(String.format("gateway.JsonFile.save()\n"));

    // Save to File
    JsonFile<JsonFileMessageDTO> example1DTOJsonFile = new JsonFile<>(JsonFileMessageDTO.class, "gateway-jsonfile-message.json");
    example1DTOJsonFile.save(jsonFileMessageDTO);

    JsonFileMessageDTO response = example1DTOJsonFile.load();

    response.getMessages().add(String.format("gateway.JsonFile.load()\n"));
    logger.info(response);

    return response;
  }

  @Synchronized
  public boolean updateRabbitMqMessage(RabbitmqMessageDTO rabbitmqMessageDTO) {

    rabbitmqMessageDTO.getMessages().add(String.format("gateway.GatewayService.updateRabbitMqMessage()\n"));

    // Send via RabbitMQ
    rabbitMqMessageDispatcher.send(rabbitmqMessageDTO);

    return true;
  }

  @Synchronized
  public RestMessageDTO updateRestMessage(RestMessageDTO restMessageDTO) {

    restMessageDTO.getMessages().add(String.format("gateway.GatewayService.updateRestMessage()\n"));

    return processServiceRESTClient.postRestMessage(restMessageDTO);
  }

  @Synchronized
  public boolean updateSocketMessage(SocketMessageDTO socketMessageDTO){

    socketMessageDTO.getMessages().add(String.format("gateway.GatewayService.updateSocketMessage()\n"));

    webSocketService.send(socketMessageDTO);

    return true;
  }

  @Synchronized
  public MongoDbMessageDTO saveMongoDbMessage(MongoDbMessageDTO mongoDbMessageDTO){
    MongoDbDocumentModel mongoDbDocumentModel = gson.fromJson(gson.toJson(mongoDbMessageDTO), MongoDbDocumentModel.class);

    mongoDbMessageDocumentDatabase.save(mongoDbDocumentModel);

    mongoDbMessageDocumentDatabase.getAll().stream().forEach(doc -> logger.info("Database \n" + doc.toString()));

    return gson.fromJson(gson.toJson(mongoDbMessageDocumentDatabase.getEntry(mongoDbDocumentModel.getId())), MongoDbMessageDTO.class);
  }

  @Synchronized
  public Boolean clearDatabase(){
    mongoDbMessageDocumentDatabase.clear();

    return true;
  }
}