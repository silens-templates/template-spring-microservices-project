package template.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@Document(collection = "mongodb_document")
public class MongoDbDocumentModel {

  @Id()
  @NonNull
  @JsonIgnore
  private ObjectId id;

  private int field1;
  private long field2;
  private float field3;
  private double field4;

  @NonNull
  private String field5;

  @NonNull
  private List<String> messages;

  public MongoDbDocumentModel() {
    this(
        new ObjectId(),
        0,
        0l,
        0.0f,
        0.0,
        "",
        new ArrayList<>()
    );
  }
}