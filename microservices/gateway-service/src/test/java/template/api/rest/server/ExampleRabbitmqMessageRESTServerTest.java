package template.api.rest.server;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import template.example_rabbitmq_message_api.RabbitmqMessageDTO;
import template.services.GatewayService;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringRunner.class)
@WebMvcTest(ExampleRabbitmqMessageRESTServer.class)
class ExampleRabbitmqMessageRESTServerTest {

  @MockBean
  private GatewayService gatewayService;

  @Autowired
  private MockMvc mvc;

  // These objects will be magically initialized by the initFields method below.
  private JacksonTester<RabbitmqMessageDTO> json;

  @BeforeEach
  public void setup() {
    JacksonTester.initFields(this, new ObjectMapper());
  }

  @Test
  public void postExample1() throws Exception {
//    given(gatewayService.updateExample1(new Example1DTO()))
//        .willReturn(true);

    MockHttpServletResponse response = mvc.perform(
        post("/post/rabbitmq_message").contentType(MediaType.APPLICATION_JSON)
            .content(json.write(new RabbitmqMessageDTO()).getJson()))
        .andReturn().getResponse();

    assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
//    assertThat(response.getContentAsString()).isEqualTo("true");
  }
}