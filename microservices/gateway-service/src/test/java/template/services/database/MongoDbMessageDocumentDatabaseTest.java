package template.services.database;

import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import template.models.MongoDbDocumentModel;

import java.util.ArrayList;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
class MongoDbMessageDocumentDatabaseTest {

  @Autowired
  private MongoDbMessageDocumentDatabase mongoDbMessageDocumentDatabase;

  private MongoDbDocumentModel mongoDbDocumentModel1;

  private MongoDbDocumentModel mongoDbDocumentModel2;

  private MongoDbDocumentModel mongoDbDocumentModel3;

  @BeforeEach
  void setup() {
    mongoDbMessageDocumentDatabase.clear();

    mongoDbDocumentModel1 = MongoDbDocumentModel.builder()
        .id(new ObjectId())
        .field1(1)
        .field2(1l)
        .field3(1.0f)
        .field4(1.0)
        .field5("1")
        .messages(new ArrayList<>(Arrays.asList("0","1","2")))
        .build();

    mongoDbDocumentModel2 = MongoDbDocumentModel.builder()
        .id(new ObjectId())
        .field1(2)
        .field2(2l)
        .field3(2.0f)
        .field4(2.0)
        .field5("2")
        .messages(new ArrayList<>(Arrays.asList("3","4","5")))
        .build();

    mongoDbDocumentModel3 = MongoDbDocumentModel.builder()
        .id(new ObjectId())
        .field1(3)
        .field2(3l)
        .field3(3.0f)
        .field4(3.0)
        .field5("3")
        .messages(new ArrayList<>(Arrays.asList("6","7","8")))
        .build();
  }

  @Test
  void countOfEmptyDatabase() {
    assertThat(mongoDbMessageDocumentDatabase.size()).isEqualTo(0);
  }

  @Test
  void countOfDatabaseSizeOne() {
    mongoDbMessageDocumentDatabase.save(mongoDbDocumentModel1);

    assertThat(mongoDbMessageDocumentDatabase.size()).isEqualTo(1);
  }

  @Test
  void countOfDatabaseSizeTwo() {
    mongoDbMessageDocumentDatabase.save(mongoDbDocumentModel1);
    mongoDbMessageDocumentDatabase.save(mongoDbDocumentModel2);

    assertThat(mongoDbMessageDocumentDatabase.size()).isEqualTo(2);
  }

  @Test
  void isInDataBase() {
    mongoDbMessageDocumentDatabase.save(mongoDbDocumentModel1);
    mongoDbMessageDocumentDatabase.save(mongoDbDocumentModel2);

    assertThat(mongoDbMessageDocumentDatabase.contains(mongoDbDocumentModel2));
  }

  @Test
  void getEntryFromDataBase() {
    mongoDbMessageDocumentDatabase.save(mongoDbDocumentModel1);
    mongoDbMessageDocumentDatabase.save(mongoDbDocumentModel2);

    assertThat(mongoDbMessageDocumentDatabase.contains(mongoDbDocumentModel1)).isEqualTo(true);
    assertThat(mongoDbMessageDocumentDatabase.contains(mongoDbDocumentModel2)).isEqualTo(true);
  }

  @Test
  void updateEntryInDatabase() {
    mongoDbMessageDocumentDatabase.save(mongoDbDocumentModel1);
    mongoDbMessageDocumentDatabase.save(mongoDbDocumentModel2);

    mongoDbDocumentModel2 = MongoDbDocumentModel.builder()
        .id(mongoDbDocumentModel2.getId())
        .field1(2)
        .field2(2l)
        .field3(2.0f)
        .field4(2.0)
        .field5("test")
        .messages(new ArrayList<>(Arrays.asList("3","4","5")))
        .build();

    mongoDbMessageDocumentDatabase.save(mongoDbDocumentModel2);

    assertThat(mongoDbMessageDocumentDatabase.size()).isEqualTo(2);
    assertThat(mongoDbMessageDocumentDatabase.getEntry(mongoDbDocumentModel2.getId()).getField5()).isEqualTo("test");
  }

  @Test
  void failedHasEntryInDatabaseChanged() {
    mongoDbMessageDocumentDatabase.save(mongoDbDocumentModel1);
    mongoDbMessageDocumentDatabase.save(mongoDbDocumentModel2);

    assertThat(mongoDbMessageDocumentDatabase.hasChanged(mongoDbDocumentModel3)).isEqualTo(false);
  }

  @Test
  void hasEntryInDatabaseChanged() {
    mongoDbMessageDocumentDatabase.save(mongoDbDocumentModel1);
    mongoDbMessageDocumentDatabase.save(mongoDbDocumentModel2);

    mongoDbDocumentModel2 = MongoDbDocumentModel.builder()
        .id(mongoDbDocumentModel2.getId())
        .field1(9)
        .field2(2l)
        .field3(2.0f)
        .field4(2.0)
        .field5("2")
        .messages(new ArrayList<>(Arrays.asList("3","4","5")))
        .build();

    assertThat(mongoDbMessageDocumentDatabase.hasChanged(mongoDbDocumentModel2)).isEqualTo(true);
  }
}