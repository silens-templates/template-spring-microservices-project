package template.app_docs;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ApiDocsController {

  @RequestMapping(value = "/")
  public String index() {
    return "redirect:swagger-ui.html";
  }
}
