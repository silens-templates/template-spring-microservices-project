package template.services.process_service_fields;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import template.api.socket.WebSocketController;
import template.example_websocket_message_api.SocketMessageDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Service
public class ServerGeneratedMessages {

  private WebSocketController webSocketController;

  private ExecutorService executor = Executors.newSingleThreadExecutor();

  @Autowired
  public ServerGeneratedMessages(WebSocketController webSocketController) {
    this.webSocketController = webSocketController;
  }

  public void initExampleEmulatedMessages001() {
    Runnable runnableTask = () -> {
      List<SocketMessageDTO> socketMessageDTOList = new ArrayList<>();
      socketMessageDTOList.add(new SocketMessageDTO(new ArrayList<>(Arrays.asList("1. Server Generated Message"))));
      socketMessageDTOList.add(new SocketMessageDTO(new ArrayList<>(Arrays.asList("2. Server Generated Message"))));
      socketMessageDTOList.add(new SocketMessageDTO(new ArrayList<>(Arrays.asList("3. Server Generated Message"))));
      socketMessageDTOList.add(new SocketMessageDTO(new ArrayList<>(Arrays.asList("4. Server Generated Message"))));
      socketMessageDTOList.add(new SocketMessageDTO(new ArrayList<>(Arrays.asList("5. Server Generated Message"))));
      socketMessageDTOList.add(new SocketMessageDTO(new ArrayList<>(Arrays.asList("6. Server Generated Message"))));

      socketMessageDTOList.stream().forEach(socketMessage -> {
        try {
          TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }

        try {
          webSocketController.sendToTopic(socketMessage);
        } catch (Exception e) {
          e.printStackTrace();
        }
      });
    };

    executor.execute(runnableTask);
    executor.shutdown();
  }
}
