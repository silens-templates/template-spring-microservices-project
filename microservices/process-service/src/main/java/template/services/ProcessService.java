package template.services;

import lombok.Synchronized;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import template.example_rabbitmq_message_api.RabbitmqMessageDTO;
import template.example_rest_message_api.RestMessageDTO;
import template.services.process_service_fields.ServerGeneratedMessages;

@Service
public class ProcessService {
  private static final Logger logger = LogManager.getLogger();

  private ServerGeneratedMessages serverGeneratedMessages;

  @Autowired
  public ProcessService(ServerGeneratedMessages serverGeneratedMessages) {
    this.serverGeneratedMessages = serverGeneratedMessages;
  }

  @Synchronized
  public void updateRabbitMqMessage(RabbitmqMessageDTO rabbitmqMessageDTO) {

    rabbitmqMessageDTO.getMessages().add(String.format("process.ProcessService.updateRabbitMqMessage()\n"));
    logger.info(rabbitmqMessageDTO);
  }

  @Synchronized
  public RestMessageDTO updateRestMessage(RestMessageDTO restMessageDTO) {
    restMessageDTO.getMessages().add(String.format("process.ProcessService.updateRestMessage()\n"));
    logger.info(restMessageDTO);

    return restMessageDTO;
  }

  public void initExampleEmulatedMessages(){
    serverGeneratedMessages.initExampleEmulatedMessages001();
  }
}