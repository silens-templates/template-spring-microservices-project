package template.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

@Configuration
public class ServiceConfiguration {
  private static final Logger logger = LogManager.getLogger();

  /**
   * Types of Scope
   *
   * @Scope("singleton")  all requests for that bean name will return the same object
   * @Scope("prototype")  return a different instance every time it is requested
   * @Scope("request")
   * @Scope("session")
   * @Scope("application")
   * @Scope("websocket")
   *
   * @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
   */

  @Bean
  @LoadBalanced
  public RestTemplate getRestTemplate() {
    return new RestTemplate();
  }

  @Bean
  public BlockingQueue requestBlockingQueue(){return new LinkedBlockingDeque();}
}