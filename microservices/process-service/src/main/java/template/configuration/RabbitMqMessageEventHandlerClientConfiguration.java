package template.configuration;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;

/**
 * Configures RabbitMQ to use events in our application.
 */
@Configuration
public class RabbitMqMessageEventHandlerClientConfiguration implements RabbitListenerConfigurer {

  @Autowired
  private DefaultMessageHandlerMethodFactory messageHandlerMethodFactory;

  @Bean
  public Queue rabbitMqMessageQueue(@Value("${example.rabbitmq.message.queue}") final String queueName) {
    return new Queue(queueName, true);
  }

  @Bean
  Binding bindingRabbitMqMessage(final Queue rabbitMqMessageQueue, final TopicExchange exchange,
                                 @Value("${example.rabbitmq.message.key}") final String routingKey) {
    return BindingBuilder.bind(rabbitMqMessageQueue).to(exchange).with(routingKey);
  }

  @Override
  public void configureRabbitListeners(final RabbitListenerEndpointRegistrar registrar) {
    registrar.setMessageHandlerMethodFactory(messageHandlerMethodFactory);
  }
}