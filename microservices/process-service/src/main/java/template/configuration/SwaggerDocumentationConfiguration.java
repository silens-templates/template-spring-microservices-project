package template.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static template.configuration.Constants.SPRINT_VERSION;

@Configuration
@EnableSwagger2
public class SwaggerDocumentationConfiguration {

  ApiInfo apiInfo() {
    return new ApiInfoBuilder().title("Process service REST API")
        .description("Process-Service REST API description.")
        .termsOfServiceUrl("")
        .version(SPRINT_VERSION)
        .contact(new Contact("Billy Clabough", "", "clab4950@gmail.com"))
        .build();
  }

  @Bean
  public Docket configureControllerPackageAndConvertors() {
    return new Docket(DocumentationType.SWAGGER_2)
        .tags(new Tag("Rest Message", "<DIV align=\"right\">Send message via REST protocol</div>"))

        .select()
        .apis(RequestHandlerSelectors.basePackage("template.api.rest")).build()
        .apiInfo(apiInfo());
  }
}
