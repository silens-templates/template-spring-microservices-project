package template.api.rest.client;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import template.process_service_clients.ClearDatabaseAPI;

@Service
public class GatewayServiceRESTClient implements ClearDatabaseAPI {
  private static final Logger logger = LogManager.getLogger();

  @Autowired
  private RestTemplate restTemplate;

  @Override
  public boolean clearDatabase() {
    return sendClearDatabaseToProcessService();
  }

  private boolean sendClearDatabaseToProcessService() {
    ResponseEntity<Boolean> response = restTemplate.postForEntity("http://gateway-service/post/clear_database", null, Boolean.class);

    return response.getBody();
  }

  private boolean postClearDatabaseFallbackDTO() {
    return false;
  }
}