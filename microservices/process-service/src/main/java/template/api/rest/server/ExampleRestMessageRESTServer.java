package template.api.rest.server;

import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import template.example_rest_message_api.ExampleRestMessageServerAPI;
import template.example_rest_message_api.RestMessageDTO;
import template.services.ProcessService;

import java.util.List;

@Service
@RestController
public class ExampleRestMessageRESTServer implements ExampleRestMessageServerAPI {
  private static final Logger logger = LogManager.getLogger();

  @Autowired
  private ProcessService processService;

  @Override
  @ApiOperation("Receive rest message from gateway-service")
  public ResponseEntity<RestMessageDTO> postRestMessage(@RequestBody RestMessageDTO restMessageDTO) {

    restMessageDTO.getMessages().add(String.format("process.ExampleRestMessageRESTServer.postRestMessage()\n"));

    return acknowledgeReceivedRestMessagePost(handleRestMessagePost(restMessageDTO));
  }

  private RestMessageDTO handleRestMessagePost(RestMessageDTO restMessageDTO) {
    return processService.updateRestMessage(restMessageDTO);
  }

  private ResponseEntity<RestMessageDTO> acknowledgeReceivedRestMessagePost(RestMessageDTO restMessageDTO) {
    return new ResponseEntity<RestMessageDTO>(restMessageDTO, null, HttpStatus.OK);
  }

  @Override
  public ResponseEntity<RestMessageDTO> getRestMessage() {
    RestMessageDTO restMessageDTO = new RestMessageDTO();
    restMessageDTO.getMessages().add(String.format("process.ExampleRestMessageRESTServer.getRestMessage()\n"));
    logger.info(restMessageDTO);

    return returnRestMessage(restMessageDTO);
  }

  private ResponseEntity<RestMessageDTO> returnRestMessage(RestMessageDTO restMessageDTO) {
    return new ResponseEntity<RestMessageDTO>(restMessageDTO, null, HttpStatus.OK);
  }
}