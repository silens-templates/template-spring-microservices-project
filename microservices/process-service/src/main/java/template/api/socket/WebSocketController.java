package template.api.socket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import template.example_websocket_message_api.SocketMessageDTO;

@Controller
public class WebSocketController {
  private static final Logger logger = LogManager.getLogger();

  @Autowired
  private SimpMessagingTemplate simpMessagingTemplate;

  @MessageMapping("/socket/socket_message")
  @SendTo("/topic/messages")
  public SocketMessageDTO send(final SocketMessageDTO socketMessageDTO) throws Exception {
    socketMessageDTO.getMessages().add(String.format("process.WebSocketController.send()\n"));
    logger.info(socketMessageDTO);

    return socketMessageDTO;
  }

  @MessageMapping("/topic/messages")
  public void sendToTopic(final SocketMessageDTO socketMessageDTO) throws Exception {
    socketMessageDTO.getMessages().add(String.format("process.WebSocketController.sendToTopic()\n"));
    logger.info(socketMessageDTO);

    simpMessagingTemplate.convertAndSend("/topic/messages", socketMessageDTO);
  }
}