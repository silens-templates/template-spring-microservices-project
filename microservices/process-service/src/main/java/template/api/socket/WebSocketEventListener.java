package template.api.socket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import template.services.ProcessService;

@Component
public class WebSocketEventListener {
  private static final Logger logger = LogManager.getLogger();

  private ProcessService processService;

  @Autowired
  public WebSocketEventListener(ProcessService processService) {
    this.processService = processService;
  }

  @EventListener
  public void handleWebSocketConnectListener(final SessionConnectedEvent sessionConnectedEvent) {
    logger.info(String.format("connected."));

    processService.initExampleEmulatedMessages();
  }

  @EventListener
  public void handleWebSocketDisconnectListener(final SessionDisconnectEvent sessionDisconnectEvent) {
    logger.info(String.format("disconnected."));
  }
}
