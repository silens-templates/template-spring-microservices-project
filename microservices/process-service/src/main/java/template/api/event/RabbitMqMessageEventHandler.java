package template.api.event;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import template.example_rabbitmq_message_api.RabbitmqMessageDTO;
import template.services.ProcessService;

/**
 * This class receives the events and triggers the associated
 * business logic.
 */
@Component
class RabbitMqMessageEventHandler {
  private static final Logger logger = LogManager.getLogger();

  private final ProcessService processService;

  @Autowired
  public RabbitMqMessageEventHandler(ProcessService processService) {
    this.processService = processService;
  }

  @RabbitListener(queues = "${example.rabbitmq.message.queue}")
  void handleSensorNetwork(final RabbitmqMessageDTO rabbitmqMessageDTO) {

    rabbitmqMessageDTO.getMessages().add(String.format("process.RabbitMqMessageEventHandler.handleSensorNetwork()\n"));

    try {
      if (rabbitmqMessageDTO != null) {
        processService.updateRabbitMqMessage(rabbitmqMessageDTO);
      }
    } catch (final Exception e) {
      // Avoids the example1DTO to be re-queued and reprocessed.
      throw new AmqpRejectAndDontRequeueException(e);
    }
  }
}