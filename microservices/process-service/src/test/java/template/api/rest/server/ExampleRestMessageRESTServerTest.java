package template.api.rest.server;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import template.TimestampUTC;
import template.example_rest_message_api.RestMessageDTO;
import template.services.ProcessService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RunWith(SpringRunner.class)
@WebMvcTest(ExampleRestMessageRESTServer.class)
class ExampleRestMessageRESTServerTest {

  @MockBean
  private ProcessService gatewayService;

  @Autowired
  private MockMvc mvc;

  // These objects will be magically initialized by the initFields method below.
  private JacksonTester<RestMessageDTO> json;

  @BeforeEach
  public void setup() {
    JacksonTester.initFields(this, new ObjectMapper());
  }

  @Test
  public void getEmitterList() throws Exception {
    RestMessageDTO restMessageDTO = new RestMessageDTO();
    restMessageDTO.getMessages().add(String.format("process.ExampleRestMessageRESTServer.getRestMessage()\n"));

    given(gatewayService.updateRestMessage(restMessageDTO))
        .willReturn(restMessageDTO);

    MockHttpServletResponse response = mvc.perform(
        get("/get/rest_message")
            .accept(MediaType.APPLICATION_JSON))
        .andReturn().getResponse();

    assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    assertThat(response.getContentAsString()).isEqualTo(json.write(restMessageDTO).getJson());
  }
}